CC = gcc
bst: bst.o
	${CC} bst.o -o bst
bst.o: bst.c
	${CC} -c bst.c
clean:
	@echo "Cleaning up ..."
	rm bst.o bst
