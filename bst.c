/* Binary search Tree in c */
#include <stdio.h>
#include<stdlib.h>

typedef struct Node Node;
struct Node {
	int data;
	Node *left, *right;
};

Node* getNode(int d) {
	Node* n=malloc(sizeof( Node));
	n->data = d; n->left = n->right = NULL;
	return n;
}
Node* insert( Node* r, int d)
{
	if(!r) return getNode(d);
	if(d<r->data) {
		r->left = insert(r->left, d);
	} else {
		r->right = insert(r->right, d);
	}
	return r;
}
Node* search( Node* r, int d)
{
	if(r==NULL || r->data == d) return r;
	return r->data>d ? search(r->left,d):search(r->right,d);
}
void inorder( Node* r)
{
	if(r != NULL) {
		inorder(r->left);
		printf("%d\n", r->data);
		inorder(r->right);
	}
}
 Node* getMinNode( Node* n)
{
	Node* curr = n;
	while(curr&&curr->left != NULL)
		curr = curr->left;
	return curr;
}
/*
 * Deletion of a Node involves following cases:
 * *Node to be deleted is leaf
 * *Node to be deleted has only one child
 * *Node to be deleted has two children
 */
Node* deleteNode( Node* r, int d)
 {
	Node* tmp;
	if(!r) return r;
	if(r->data>d){
		r->left = deleteNode(r->left, d);
	} else if(r->data<d) {
		r->right = deleteNode(r->right, d);
	} else {
		/* -- if d is same as the root's data--*/
		/* Cond. 1&2 */
		if(!r->left) {
		tmp = r->right;
		free(r);
		return tmp;
	} else if(!r->right){
		tmp = r->left;
		free(r);
		return tmp;
	}
	/* Cond. 3*/
	/* get the min value in the right subtree*/
	tmp = getMinNode(r->right);
	r->data = tmp->data;         /* copy over to root */
	/* delete that min node*/
	r->right = deleteNode(r->right, tmp->data);
	}
	return r;
 }
int main()
{
	Node* root = NULL;
	root = insert(root, 10);
	root = insert(root, 55);
	root = insert(root, 14);
	root = insert(root, 30);
	root = insert(root, 90);
	root = insert(root, 6);
	inorder(root);
	/*
	Node* res = search(root, 14);
	printf("%d\n", res->data);
	*/
	printf("Node 10 is deleted:\n");
	root = deleteNode(root, 10);
	inorder(root);
	return 0;
}
